import os
from collections import namedtuple

TWITCH_CLIENT_ID = os.environ.get('TWITCH_CLIENT_ID')
TWITCH_CLIENT_SECRET = os.environ.get('TWITCH_CLIENT_SECRET')
BOT_NICK = os.environ.get('BOT_NICK')
BOT_PASS = os.environ.get('BOT_PASS')
CHANNELS = os.environ.get('CHANNELS', "")

Bot = namedtuple("Bot", ["NICK", "PASS"])

bots: list[Bot] = [Bot(NICK=BOT_NICK, PASS=BOT_PASS)]
twitch_client_id: str = TWITCH_CLIENT_ID
twitch_client_secret: str = TWITCH_CLIENT_SECRET
channels: list[str] = [i.strip() for i in CHANNELS.split(",")]
