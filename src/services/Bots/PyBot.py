import asyncio
import logging
import re
from typing import Optional, Set

from src.utils import prettytwitch
from src.utils.prettytwitch.utils.TypesEnum import TypesEnum

debug_timeout = 60 * 10


class PyBot:
    def __init__(self,
                 host: str = "irc.chat.twitch.tv",
                 port: int = 6667,
                 bot_name: str = None,
                 password: str = None):
        self.host: str = host
        self.port: int = port
        self.bot_name: str = bot_name
        self.password: str = password

        self.channels: Set[str] = set()

        self.reader: Optional[asyncio.StreamReader] = None
        self.writer: Optional[asyncio.StreamWriter] = None

        self.is_debug = False
        self.debug_channel = ""

    async def send_raw(self, raw: str):
        self.writer.write(f"{raw}\r\n".encode("utf-8"))

    async def connect(self):
        self.reader, self.writer = await asyncio.open_connection(self.host, self.port)
        await self.send_raw("CAP REQ :twitch.tv/membership")
        await self.send_raw("CAP REQ :twitch.tv/tags")
        await self.send_raw("CAP REQ :twitch.tv/commands")
        await self.send_raw(f"PASS {self.password}")
        await self.send_raw(f"NICK {self.bot_name}")

        asyncio.create_task(self.listen())

    def disconnect(self):
        self.writer.close()

    async def join_channel(self, chan: str):
        print(chan)
        await self.send_raw(f"JOIN #{chan}")
        self.channels.add(chan)
        await asyncio.sleep(0.5)

    async def part_channel(self, chan: str):
        await self.send_raw(f"PART #{chan}")
        if chan in self.channels:
            self.channels.remove(chan)

    async def mess_chan(self, chan: str, mess: str):
        await self.send_raw(f"PRIVMSG #{chan} :{mess}")

    async def read_line(self) -> Optional[bytes]:
        return await self.reader.readline()

    async def listen(self):
        while True:
            try:
                await self.process_loop()
            except Exception as e:
                logging.error(e)
                exit(1) # Падаем, контейнер перезапустится и всё будет круто 5Head. В смысле нужно обрабатывать ошибки?

    async def process_loop(self):
        resp_cor = await self.read_line()
        resp = resp_cor.decode("utf-8")

        if resp == "":
            logging.error("Empty resp from twitch socket")
            exit(1)

        pt = prettytwitch.Response(resp)

        if self.is_debug:
            if self.debug_channel == "" or pt.channel is None or self.debug_channel == pt.channel:
                logging.debug(resp)

        if pt.type == TypesEnum.UNDEFINED:
            logging.error("")
            logging.error("UNDEFINED RESP TYPE")
            logging.error(resp)
            logging.error("")

        # System Messages
        if pt.type == TypesEnum.CONNECTION:
            pass
        elif pt.type == TypesEnum.USER_LIST:
            pass
        elif pt.type == TypesEnum.USER_LIST_END:
            pass
        elif pt.type == TypesEnum.RECONNECT:
            pass
        elif pt.type == TypesEnum.PING:
            await self.send_raw("PONG :tmi.twitch.tv")
            pass
        elif pt.type == TypesEnum.PONG:
            pass

        # Invalid IRC Command
        elif pt.type == TypesEnum.INVALID_CMD:
            pass

        # Twitch specific IRC Requests
        elif pt.type == TypesEnum.CAPABILITY_REG:
            pass

        # Generic
        elif pt.type == TypesEnum.JOIN:
            pass
        elif pt.type == TypesEnum.PART:
            pass
        elif pt.type == TypesEnum.PRIVMSG:
            if "BadBatBot debug start".lower() in pt.message.lower():
                await asyncio.sleep(1)

                patt = re.compile(r'badbatbot debug start(.*)')
                inspection = re.search(patt, pt.message.lower())
                self.debug_channel = inspection.group(1).strip()
                asyncio.create_task(self.debug_start())

                if self.debug_channel == "":
                    message = f"{pt.user}, debug mod is activated!"
                else:
                    message = f"{pt.user}, debug mod on channel {self.debug_channel} is activated!"
                await self.mess_chan(pt.channel, message)
            elif "BadBatBot debug stop".lower() in pt.message.lower():
                await asyncio.sleep(1)
                asyncio.create_task(self.debug_stop())
                await self.mess_chan(pt.channel, f"{pt.user}, debug mod is stop!")
            elif "BadBatBot".lower() in pt.message.lower():
                await asyncio.sleep(1)
                await self.mess_chan(pt.channel, f"{pt.user}, i'm here!")

        # Capability: Membership
        elif pt.type == TypesEnum.OP:
            pass
        elif pt.type == TypesEnum.DEOP:
            pass

        # Capability: Commands
        elif pt.type == TypesEnum.NOTICE:
            pass
        elif pt.type == TypesEnum.HOSTSTART:
            pass
        elif pt.type == TypesEnum.HOSTEND:
            pass
        elif pt.type == TypesEnum.TIMEOUT:
            pass
        elif pt.type == TypesEnum.BAN:
            pass
        elif pt.type == TypesEnum.CLEARCHAT:
            pass
        elif pt.type == TypesEnum.USERSTATE:
            pass
        elif pt.type == TypesEnum.ROOMSTATE:
            pass
        elif pt.type == TypesEnum.USERNOTICE:
            pass
        elif pt.type == TypesEnum.SUB:
            pass
        elif pt.type == TypesEnum.GLOBALUSERSTATE:
            pass

    async def debug_start(self):
        self.is_debug = True
        await asyncio.sleep(debug_timeout)
        await self.debug_stop()

    async def debug_stop(self):
        self.is_debug = False
        self.debug_channel = ""
