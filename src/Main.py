from src.services.Bots import BotsService


async def main():
    bots = BotsService()

    await bots.run()
